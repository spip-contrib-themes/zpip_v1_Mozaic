<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'choix_d_une_variante' => "Choix d'une variante de thème",
	'label_variante'=>"Variante du thème",

);
